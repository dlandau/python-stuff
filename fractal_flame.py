#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt
from functools import partial

def radius(x,y):
    return np.sqrt(x*x + y*y)

def theta(x,y):
    return np.arctan2(x,y)

def phi(x,y):
    return np.arctan2(y,x)

def linear(x,y):
    return x,y
def sinusoidal(x,y):
    return np.sin(x), np.sin(y)
def spherical(x,y):
    rinv = 1./radius(x,y)
    rinvsq = rinv*rinv
    return rinvsq*x, rinvsq*y
def swirl(x,y):
    r = radius(x,y)
    rsq = r*r
    return x*np.sin(rsq) - y*np.cos(rsq), x*np.cos(rsq) + y*np.sin(rsq)
def horseshoe(x,y):
    rinv = 1./radius(x,y)
    return rinv * (x-y)*(x+y), rinv * 2* x*y
def polar(x,y):
    return (theta(x,y) / np.pi, radius(x,y) - 1)
def handkerchief(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return (r*np.sin(th+r), r * np.cos(th - r))
def heart(x,y):
    r, th = radius(x,y), theta(x,y)
    return (r* np.sin(th*r), -r*np.cos(th*r))
def disc(x,y):
    r, th = radius(x,y), theta(x,y)
    return th/np.pi * np.sin(np.pi*r), th/np.pi*np.cos(np.pi*r)
def spiral(x,y):
    r, th = radius(x,y), theta(x,y)
    return 1/r * (np.cos(th) + np.sin(r)), 1/r * (np.sin(th) - np.cos(r))
def hyperbolic(x,y):
    r, th = radius(x,y), theta(x,y)
    return np.sin(th)/r, r*np.cos(th)

def affine_transform(x,y, a=0., b=0., c=0., d=0., e=0., f=0.):
    return a*x+b*y+c, d*x+e*y+f

def F(x,y, v, V, a=0., b=0., c=0., d=0., e=0., f=0.):
    Fsum = [0.0, 0.0]
    for i in range(len(v)):
        affx, affy = affine_transform(x, y, a,b,c,d,e,f)
        Ftmp = V[i](affx, affy)
        Fsum[0] += v[i] * Ftmp[0]
        Fsum[1] += v[i] * Ftmp[1]
    return Fsum

def weightedRandomInt( weights ):
    n = len(weights)
    cumsum = np.cumsum(weights)
    wsum = np.sum(weights)
    x = np.random.uniform(low=0, high=wsum)
    for i in xrange(n):
        if cumsum[i] > x:
            return i
    raise Exception

def main():
    n = 1000
    def xy2coords(xy):
        return (int((xy[0]+1)*n/2.0),int( (xy[1]+1)*n/2.0))
    hist = np.zeros((n,n))
    colors = np.zeros((n,n,3))
    Fs = [(partial(F, v = [0.9, 0.3, 0.2], V = [heart, spiral, linear], a = 1, e = 1), 0.8, np.array([1.,0.,0.])),
          (partial(F, v = [0.5, 0.2, 0.3], V = [disc, hyperbolic, horseshoe], a = 1, e = 1), 0.6, np.array([0.1,0.1,0.8])),
          (partial(F, v = [0.5, 0.2, 0.3], V = [polar, hyperbolic, horseshoe], a = 1,b=.4, d = 1, e =-.2), 0.3, np.array([1,222./255,101./255])),
          (partial(F, v = [0.8, 0.1, 0.1], V = [sinusoidal, spherical, handkerchief], a = 1, e = 1), 0.2, np.array([0.9,0.1,0.1]))
        ]

# random walk:
#     step = .002
#     Fs = [(partial(F, v = [1.0], V = [linear], a = 1., c = step, e = 1., f = 0.), 0.1, np.array([0.2,0.4,0.2])),
#           (partial(F, v = [1.0], V = [linear], a = 1., c = -step, e = 1., f = 0.), 0.1, np.array([0.1,0.8,0.5])),
#           (partial(F, v = [1.0], V = [linear], a = 1., c = 0., e = 1., f = step), 0.1, np.array([0.1,0.8,0.5])),
#           (partial(F, v = [1.0], V = [linear], a = 1., c = 0., e = 1., f = -step), 0.1, np.array([0.9,0.1,0.1]))
# ]

    xy = np.random.uniform(low=-1, high=1, size=2)

    outside = 0
    iterations = 5000000
    for _ in xrange(iterations):
        i = weightedRandomInt(zip(*Fs)[1])
        xy = np.array(Fs[i][0](xy[0], xy[1]))
        try:
            hist[xy2coords(xy)] += 1
            colors[xy2coords(xy)] = (colors[xy2coords(xy)] + Fs[i][2]) / 2.0
        except:
            outside += 1
#    print outside

    hist += 1.0 # avoid log of zero
    maxlog = np.log(np.max(hist))
    hist = np.log(hist) / maxlog
    im = hist.reshape((n,n,1)) * colors
#    im = np.zeros((n,n,4))
#    im[:,:,:3] = colors
#    im[:,:,3:] = hist.reshape((n,n,1))
    f = plt.figure()
    ax = f.add_subplot(111)
    ax.imshow(im, interpolation="nearest")
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.margins(0,0)

    plt.show()
    plt.savefig("fractal_"+ str(hash(f)) + ".eps", bbox_inches="tight", pad_inches=0.0 )
#    return hist

if __name__ == "__main__":
    main()
