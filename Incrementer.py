class Incrementer(object):
    def incrementString(self,s):
        if not s.isalpha():
            raise ValueError
        def add1(c):
            return ('A', True) if c == 'Z' else (chr(ord(c) + 1), False)
        self.overflowed = True
        def f(acc, c):
            newc, self.overflowed = add1(c) if self.overflowed else (c, False)
            return acc + newc
        result = "".join(reversed(reduce(f, reversed(s.upper()), "")))
        return ("A" if self.overflowed else "") + result


