#!/usr/bin/env python
# Copyright (c) 2007-8 Qtrac Ltd. All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License, or
# version 3 of the License, or (at your option) any later version. It is
# provided for educational purposes and is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

from __future__ import division
import sys
from math import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *


class Form(QDialog):

    def __init__(self, parent=None, app=None):
        super(Form, self).__init__(parent)
        self.browser = QTextBrowser()
        self.lineedit = QLineEdit("Type an expression and press Enter")
        self.lineedit.selectAll()
        self.quit_button = QPushButton("quit")
        self.calc_button = QPushButton("eval")
        layout = QVBoxLayout()
        inner_layout = QHBoxLayout()
        layout.addWidget(self.browser)
        inner_layout.addWidget(self.lineedit)
        inner_layout.addWidget(self.calc_button)
        inner_layout.addWidget(self.quit_button)
        layout.addLayout(inner_layout)
        self.setLayout(layout)
        self.lineedit.setFocus()
#        self.connect(self.lineedit, SIGNAL("returnPressed()"),
#                     self.updateUi)
        self.connect(self.calc_button, SIGNAL("pressed()"),
                     self.updateUi)
        self.connect(self.quit_button, SIGNAL("pressed()"),
                     app.quit)
        self.setWindowTitle("Calculate")


    def updateUi(self):
        try:
            text = unicode(self.lineedit.text())
            self.browser.append("%s = <b>%s</b>" % (text, eval(text)))
        except:
            self.browser.append(
                    "<font color=red>%s is invalid!</font>" % text)


app = QApplication(sys.argv)
form = Form(app=app)
form.show()
app.exec_()

