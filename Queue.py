
class Queue:
    """A First In First Out (FIFO) queue."""

    def __init__(self):
        self.start = None
        self.end   = None

    def enqueue(self, data):
        if not self.start:
            self.start = QueueNode(data)
            self.end   = self.start
        else:
            self.end.next = QueueNode(data)
            self.end.next.previous = self.end
            self.end.next.next = self.start
            self.start.previous = self.end.next
            self.end = self.end.next

    def dequeue(self):
        if not self.start:
            raise ValueException("Can't dequeue from an empty queue")
        else:
            result = self.start.data
            if not self.start.next:
                self.start = None
                self.end = None
            elif self.start.next == self.end:
                self.start = self.end
                self.end.next = None
                self.end.previous = None
            else:
                self.start.next.previous = self.end
                self.end.next = self.start.next
                self.start = self.start.next
            return result

    def is_empty(self):
        return self.start == None


class QueueNode:
    """A node in a doubly linked list"""
    
    def __init__(self, data):
        self.next      = None
        self.previous  = None
        self.data      = data

def main(*args):
    
    queue = Queue()
    for i in reversed(range(10)):
        queue.enqueue(i)

    while not queue.is_empty():
        print queue.dequeue()


if __name__ == '__main__':
    main()

