""" Code example from Complexity and Computation, a book about
exploring complexity science with Python.  Available free from

http://greenteapress.com/complexity

Copyright 2011 Allen B. Downey.
Distributed under the GNU General Public License at gnu.org/licenses/gpl.html.
"""

import itertools
import copy
import collections


class Vertex(object):
    """A Vertex is a node in a graph."""

    def __init__(self, label=''):
        self.label = label

    def __repr__(self):
        """Returns a string representation of this object that can
        be evaluated as a Python expression."""
        return 'Vertex(%s)' % repr(self.label)

    __str__ = __repr__
    """The str and repr forms of this object are the same."""


class Edge(tuple):
    """An Edge is a list of two vertices."""

    def __new__(cls, *vs):
        """The Edge constructor takes two vertices."""
        if len(vs) != 2:
            raise ValueError( 'Edges must connect exactly two vertices.')
        return tuple.__new__(cls, vs)

    def __repr__(self):
        """Return a string representation of this object that can
        be evaluated as a Python expression."""
        return 'Edge(%s, %s)' % (repr(self[0]), repr(self[1]))

    __str__ = __repr__
    """The str and repr forms of this object are the same."""


class Graph(dict):
    """A Graph is a dictionary of dictionaries.  The outer
    dictionary maps from a vertex to an inner dictionary.
    The inner dictionary maps from other vertices to edges.
    
    For vertices a and b, graph[a][b] maps
    to the edge that connects a->b, if it exists."""

    def __init__(self, vs=[], es=[]):
        """Creates a new graph.  
        vs: list of vertices;
        es: list of edges.
        """
        for v in vs:
            self.add_vertex(v)
            
        for e in es:
            self.add_edge(e)

    def add_vertex(self, v):
        """Add a vertex to the graph."""
        self[v] = {}

    def add_edge(self, e):
        """Adds and edge to the graph by adding an entry in both directions.

        If there is already an edge connecting these Vertices, the
        new edge replaces it.
        """
        v, w = e
        self[v][w] = e
        self[w][v] = e

    def get_edge(self, v, w):
        """Returns the edge between v and w, or None."""

        try:
            return self[v][w]
        except:
            return None

    def remove_edge(self, e):
        """Removes an edge from the graph"""

        for k1, v1 in self.items():
            for k2, v2 in v1.items():
                if self[k1][k2] == e:
                    del(self[k1][k2])
                    
    def vertices(self):
        """Returns a list of all the vertices"""
        return self.keys()

    def edges(self):
        """Return a list of all the edges"""
        result = []
        for k1, v1 in self.items():
            for k2, v2 in v1.items():
                if self[k1][k2] not in result:
                    result.append(self[k1][k2])
        return result

    def out_vertices(self,v):
        """returns a list of the adjacent vertices (the ones connected to v by an edge)"""
        return self[v].keys()

    def out_edges(self,v):
        """returns a list of the connected edges"""
        return self[v].values()

    def is_edgeless(self):
        """ Returns True if the graph contains no edges, False othervise"""
        vs = self.vertices()
        for v in vs:
            if len(self[v]) != 0:
                return False
        return True

    def add_all_edges(self):
        """Makes a complete graph, raises an Exception, if some edges already exist"""
        if not self.is_edgeless():
            raise Exception("The graph shouldn't contain any edges.")
        vs = self.vertices() 
        for v1 in vs:
            for v2 in vs:
                if v1 != v2:
                    self.add_edge(Edge(v1,v2))

    def add_regular_edges(self, degree):
        """Makes a regular graph of degree 'degree', raises an Exception, if some edges already exist"""
        if not self.is_edgeless():
            raise Exception("The graph shouldn't contain any edges.")
        if degree >= len(self):
            raise Exception("Can't make a regular graph with degree of nodes greater than number of nodes")
        if len(self) * degree % 2 != 0:
            raise Exception("Can't make a regular graph when n * k is not divisible with two.")
        vs = self.vertices()
        if degree == 0:
            return
        g = self.add_regular_impl(degree, self.vertices(), Graph(self.vertices()), True)
        for e in g.edges():
            self.add_edge(e)

    def degree(self, v):
        """Returns the degree of a vertex"""
        return len(self[v])

    def add_regular_impl(self, degree, vs_in, graph, first_level=False):
        vs = copy.copy(vs_in)
        v = vs.pop()
        if len(vs) == 0:
            return graph
        if degree - graph.degree(v) == 0:
            return self.add_regular_impl(degree, vs, graph)
        def f(comb):
            g = Graph(graph.vertices(), graph.edges())
            edges = []
            for w in comb:
                if (g.degree(w) < degree):
                    edges.append(Edge(v,w))
            if len(edges) == degree - graph.degree(v):
                for e in edges:
                    g.add_edge(e)
                result = self.add_regular_impl(degree, vs, g)
                if result.is_regular():
                    return result
                else:
                    return None
#        for comb in itertools.combinations( vs, degree - graph.degree(v)):
        results = []
#        if first_level:
#            from multiprocessing import Pool
#            pool = Pool(processes=4)
#            results = pool.map(f, itertools.combinations( vs, degree - graph.degree(v)))
#        else:
        results = map(f, itertools.combinations( vs, degree - graph.degree(v)))
        results = filter(None, results)
        if len(results) > 0: return results[0]
            # g = Graph(graph.vertices(), graph.edges())
            # edges = []
            # for w in comb:
            #     if (g.degree(w) < degree):
            #         edges.append(Edge(v,w))
            # if len(edges) == degree - graph.degree(v):
            #     for e in edges:
            #         g.add_edge(e)
            #     result = self.add_regular_impl(degree, vs, g)
            #     if result.is_regular():
            #         return result
        return graph

    
    def is_regular(self):
        """Returns True if graph is regular (i.e. all vertices have the same degree), False otherwise"""
        return len(set( map( self.degree, self.vertices()))) == 1 

    def bfs(self, visit=None):
        visited = set()
        queue = collections.deque()
        def process(v):
            if visit: visit(v)
            visited.add(v)
            queue.append(v)            
        v = self.vertices()[0]
        process(v)
        while len(queue) > 0:
            v = queue.popleft()
            for w in self.out_vertices(v):
                if w not in visited:
                    process(w)
                # else:
                #     try:
                #         if self.get_edge(v,w).asd > 1:
                #             self.remove_edge(self.get_edge(v,w))
                #         else:
                #             self.get_edge(v,w).asd += 1
                #     except:
                #         self.get_edge(v,w).asd = 0
        return visited

    def is_connected(self):
        """Return True if graph is connected, False otherwise"""
        vs = []
        def visit( v ):
            vs.append(v)
        marks = self.bfs(visit)
        if len(marks) == len(self.vertices()):
            return True
        else:
            return False

    def remove_edges(self):
        """Removes all edges"""
        for v in self.itervalues():
            v.clear()

def main(script, *args):
    # v = Vertex('v')
    # print v
    # w = Vertex('w')
    # print w
    # e = Edge(v, w)
    # print e
    # g = Graph([v,w], [e])
    # print g

    # x = Vertex('x')
    # g = Graph([v,w,x], [e])
    # print g.get_edge(v,w)
    # print g.get_edge(v,x)
    
    # g.remove_edge(e)
    # print g

    # print g.vertices()

    # e2 = Edge(v,x)
    # g = Graph([v,w,x], [e,e2])
    # print g.edges()

    # print g.out_vertices(x)
    # print g.out_vertices(v)
    # g = Graph([v,w,x],[])
    # g.add_all_edges()
    # print g

    if __name__ == '__main__':
        import sys
        main(*sys.argv)
