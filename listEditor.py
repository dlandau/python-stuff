#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
import sys

class EditList(QtGui.QDialog):
    def __init__(self, name="", initial_list = []):
        super(EditList, self).__init__()
        self.name = name
        l = QtGui.QHBoxLayout()
        buttons_layout = QtGui.QVBoxLayout()

        self.button_names_ordered = ["add", "edit", "remove", "up", "down", "sort", "close"]
        self.button_names = dict( add="&Add...", edit="&Edit...", remove="&Remove...",
                         up="&Up", down="&Down", sort="&Sort", close="Close")
        self.buttons = [QtGui.QPushButton(self.button_names[key]) for key in self.button_names_ordered]
        for button_index in range(len(self.button_names_ordered)):
            button = self.buttons[button_index]
            buttons_layout.addWidget(button)
        self.list_ = QtGui.QListWidget()
        if initial_list:
            self.list_.addItems(initial_list)
        self.setWindowTitle("Edit %s%sList" % (name, " " if len(name) > 0 else ""))
        l.addWidget(self.list_)
        l.addLayout(buttons_layout)
        self.setLayout(l)
        for i, button_name in enumerate(self.button_names_ordered):
            func = eval("self.%s" % button_name)
            self.connect(self.buttons[i], QtCore.SIGNAL("pressed()"), func)

    def add(self):
        text = "Add %s" % self.name
        to_add, ok = QtGui.QInputDialog.getText(self,text, text) 
        if ok:
            self.list_.addItem(to_add)
    def edit(self):
        text = QtCore.QString("Edit %s" % self.name)
        item = self.list_.currentItem()
        to_add, ok = QtGui.QInputDialog.getText(self, text, text, text = item.text()) 
        if ok:
            pos = self.list_.currentRow()
            self.list_.takeItem(pos)
            self.list_.insertItem( pos, to_add)
            
            self.list_.setCurrentRow(pos)
    def remove(self):
        self.list_.takeItem(self.list_.currentRow())
    def up(self):
        pos = self.list_.currentRow()
        item = self.list_.takeItem(pos)
        pos = 0 if pos == 0 else pos - 1
        self.list_.insertItem(pos, item)
        self.list_.setCurrentRow(pos)
    def down(self):
        item = self.list_.takeItem(self.list_.currentRow())
        self.list_.insertItem(self.list_.currentRow() + 1, item)
        self.list_.setCurrentRow(self.list_.currentRow() + 1)
    def sort(self):
        self.list_.sortItems()
        

app = QtGui.QApplication(sys.argv)
l = EditList("Moi", [QtCore.QString(s) for s in ["moi", "hei", u"Päivää", "Iltaa", "Huomenta"]])
l.show()

app.exec_()
