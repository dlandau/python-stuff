# -*- coding: utf-8 -*-
import random

from Graph import Graph, Edge

class RandomGraph(Graph):

    def add_random_edges(self, p):
        """Makes an Erdős-Rényi random graph starting from an empty one"""
        if not self.is_edgeless():
            raise Exception("The graph shouldn't contain any edges.")
        for i, v in enumerate(self.vertices()):
            for w in self.vertices()[i+1:]:
                if random.random() < p:
                    self.add_edge( Edge(v, w))
