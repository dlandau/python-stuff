import string

def natural_numbers():
    i = 0
    while True:
        yield i
        i = i + 1

def positive_integers():
    iter = natural_numbers()
    iter.next()
    while True:
        yield iter.next()

def alphanumeric_cycle():
    for i in positive_integers():
        for c in string.ascii_lowercase:
            yield ("%s%d" % (str(c), i))
