#!/usr/bin/env python

import sys
from PyQt4 import QtGui, QtCore

class Interest(QtGui.QDialog):
      """Calculates the compound interest of an initial saving assuming the
      interest is calculated yearly"""

      def __init__(self):
            super(Interest, self).__init__()
            self.labels = {}
            self.spinboxes = {}
            self.label_texts = ["Principal:", "Rate:", "Years:", "Amount" ]
            for label_text in self.label_texts:
                  self.labels[label_text] = QtGui.QLabel(label_text)
            self.spinboxes[ "Principal:" ] = QtGui.QDoubleSpinBox()
            self.spinboxes[ "Principal:" ].setPrefix("$ ")
            self.spinboxes[ "Principal:" ].setRange(0.01, sys.float_info.max)
            self.spinboxes[ "Principal:" ].setValue(2000.0)
            self.spinboxes[ "Rate:" ] = QtGui.QDoubleSpinBox()
            self.spinboxes[ "Rate:" ].setSuffix(" %")
            self.spinboxes[ "Rate:" ].setRange(0.0, 100.0)
            self.spinboxes[ "Rate:" ].setValue(5.25)
            self.spinboxes[ "Years:" ] = QtGui.QSpinBox()
            self.spinboxes[ "Years:" ].setSuffix(" years")
            self.spinboxes[ "Years:" ].setRange(1, int(2**31 - 1))
            self.spinboxes[ "Years:" ].setValue(2)
            self.amount = QtGui.QLabel("$ 2215.51")

            layout = QtGui.QGridLayout()
            for i, label_text in enumerate(self.label_texts[:-1]):
                  layout.addWidget(self.labels[label_text], i, 0)
                  layout.addWidget(self.spinboxes[label_text], i, 1)
            layout.addWidget(self.labels[ self.label_texts[-1]], len(self.label_texts) - 1, 0)
            layout.addWidget(self.amount, len(self.label_texts) - 1, 1)
            self.setLayout(layout)
            self.setWindowTitle("Interest")

            for label_text in self.label_texts[:-2]:
                  self.connect(self.spinboxes[ label_text ], QtCore.SIGNAL("valueChanged(double)"), self.updateAmount)
                  self.connect(self.spinboxes[ "Years:" ], QtCore.SIGNAL("valueChanged(int)"), self.updateAmount)
      def updateAmount(self):
            principal = self.spinboxes[ "Principal:" ].value()
            rate = self.spinboxes[ "Rate:" ].value()
            years = self.spinboxes[ "Years:" ].value()
            amount = principal * ((1 + (rate / 100.0)) ** years)
            self.amount.setText("$ %0.2f" % round(amount,2))

app = QtGui.QApplication(sys.argv)
interest = Interest()
interest.show()
app.exec_()
            
