import Graph
from  GraphWorld import *
import string
from Graph import Vertex
from Graph import Edge
from Graph import Graph
from RandomGraph import RandomGraph
# create n Vertices
def main(script, n = '10', degree = '1',  *args):
    
    n = int(n)
    degree = int(degree)
    labels = string.ascii_lowercase + string.ascii_uppercase
    vs = [Vertex(c) for c in labels[:n]]

    g = Graph(vs)
    if len(args) >= 2:
        if args[0] == "random":
            p = float(args[1])
            g = RandomGraph(vs)
            g.add_random_edges(p)
    else:
        import time
        t1 = time.time()
        g.add_regular_edges(degree)
        t2 = time.time()
        print ("%s took %0.3f ms" % ("add_regular_edges", (t2-t1)*1000))
        if not g.is_regular():
            raise Exception("add_regular_edges() doesn't work.")
    layout = CircleLayout(g)
#    layout = BinaryTreeLayout(g)
#    layout = RandomLayout(g)

    print (g.is_connected())
    gw = GraphWorld()
    gw.show_graph(g, layout)
    gw.mainloop()


if __name__ == '__main__':
    import sys
    main(*sys.argv)
