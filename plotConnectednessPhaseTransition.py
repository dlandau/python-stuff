import matplotlib.pyplot as plt
import numpy as np
from Graph import *
from RandomGraph import RandomGraph
import string
import os


max_tries = 100
p = np.arange(0, 1, 0.01)
success_rate = np.zeros(len(p))
labels = string.ascii_lowercase + string.ascii_uppercase
#n = 5
i = 0
if not "plots" in os.listdir("."):
    os.mkdir("plots")
for n in range(2,20):
    vs = [Vertex(c) for c in labels[:n]]
    g = RandomGraph(vs)
    for i in range(len(p)):
        succesfull_tries = 0
        for _ in range(max_tries):
            g.remove_edges()
            g.add_random_edges(p[i])
            if g.is_connected():
                succesfull_tries += 1
        success_rate[i] = float(succesfull_tries) / float(max_tries)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(p, success_rate)
    plt.savefig(("plots/success_%03d.png" % n))
    

